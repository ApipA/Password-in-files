#!/usr/bin/python
#-- coding: iso-8859-1 -
# -- coding: utf-8 -

######## IMPORTS ########

import os
import docx
##from PyPDF2 import PdfFileReader
import csv

######## end IMPORTS ########

######## VARS ########

## Directory to scan
##SCAN_DIR = (r"C:\Users\Samantha.bizineche\Desktop\Test")
##SCAN_DIR = (r"C:\Users\Julien\Desktop\python\password-in-files\tests")
SCAN_DIR = os.path.abspath(os.path.split(__file__)[0])

## Keywords to find
Mots = ["Password", "password", "PASSWORD", "Mot de passe", "mot de passe", "MOT DE PASSE", "Mot de Passe", "Mot De Passe", "Pass", "pass", "PASS", "Identifiant\xa0", "identifiant", "Identifiants", "identifiants", "Login", "login", "Credentials", "credentials", "CREDENTIALS"]

contenu = []
content = ""

## Keep passwords and path in memory
passList = []
titre = ["Source", "Password"]
passList.append(titre)

## Files Extensions
extensionDOCX = ".docx"
extensionTXT = ".txt"
extensionPDF = ".pdf"

######## end VARS ########

######## FUNCTIONS ########

def scanDOCX(FILE):

    f = open(FILE, "rb")
    d = docx.Document(f)
    for p in d.paragraphs:
        contenu = [p.text]
        for ligne in contenu:
    ##        print("Print 1 : ", ligne)
            for mot in Mots :
               if mot in ligne:
                   item = [FILE, ligne]
                   passList.append(item)
                   break
    f.close()

def scanTXT(FILE):
    f = open(FILE, "r", errors="ignore")
    for ligne in f:
        for mot in Mots :
            if mot in ligne:
                item = [FILE, ligne]
                passList.append(item)
                break
    f.close()

##def scanPDF(FILE):
##    f = open(FILE, "rb")
##    pdf = PdfFileReader(f)
##    nb_pages = pdf.numPages
##    for page in range(nb_pages):
##        content = pdf.getPage(page).extractText()
####        content = content.replace("\n", "")
##        ligne = content.split("\n")
##        #print(ligne)
##        for mot in Mots :
##            if mot in ligne :
##                print(ligne)
##                break
##    f.close()

######## end FUNCTIONS ########

######## MAIN PROGRAM ########

for path, dirs, files in os.walk(SCAN_DIR, followlinks=False):
    for filename in files:

##        Concatenation du path+nom pour obtenir le chemin absolu du fichier
        FILE = os.path.join(path, filename)

##        Si cest un lien symbolique on passe au fichier suivant
        if os.path.islink(FILE):
            continue


##        Si le fichier commence par "~$" on ne le scan pas car il est illisible
        if filename.startswith("~$"):
            continue

##        Si le fichier est un DOCX
        elif filename.endswith(extensionDOCX):
            print(FILE)
            scanDOCX(FILE)

##        Sinon si le fichier est un PDF
##        elif filename.endswith(extensionPDF):
##            print(FILE)
##            scanPDF(FILE)

##        Sinon si le fichier est un TXT
        elif filename.endswith(extensionTXT):
            print(FILE)
            scanTXT(FILE)

##        Sinon fichier suivant (car ni .DOCX, ni .PDF, ni .TXT)
        else:
            continue

##print(passList)

## Write passwords found in csv file
with open('passwordScan.csv', 'w', newline='') as f:
    writer = csv.writer(f, delimiter=';')
    writer.writerows(passList)

######## end MAIN PROGRAM ########